var express = require('express');
var app = express()
var jwt = require('jwt-simple');
var url = require('url');

var token = ' ' //initialize this as a global token so it is accessible for login and logout destroy token.
var i = 0; //This is just a variable that will be used to create a unique session ID for jti. Can place any kind of code that will create a unique ID.

// backup solution login
// https://groupebizness.sisensepoc.com/app/account#/login

function redirectToJwt(request, response, user) {
  var payload;
  var jti = "cdgvdea" + i;
  i++;
  var payload = {
    iat: Math.floor((new Date().getTime() / 1000)),
    // sub: "aa.feff@jump-biz.com",
    sub: "sebastienm@jump-biz.com",
    jti: jti,
  };

  var token = jwt.encode(payload, "f0795e9f3cbf782cb1b80e80b31da5aba8244940bffbc42e4111e43247e5634d", algorithm="HS256");
  var redirect = 'https://groupebizness.sisensepoc.com/jwt?jwt=' + token; //where ip or site name should be placed here

  response.writeHead(302, {
    'Location': redirect
  });
  response.end();
}

app.get('/api/login', function (request,response){
  var query = url.parse(request.url, true).query;
  redirectToJwt(request,response);
});

app.get('/api/logout', function (request,response){
  console.log("Logout");
  //Insert a token destroy function here

  response.writeHead(302, {
    'Location': 'http://www.sisense.com'
  });
  response.end();
});

app.listen(7000); //Example of a port which Sisense will connect to access SSO Handler.
console.log('ready');
