import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare const Sisense: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'test-sisense';
  serverName = 'https://groupebizness.sisensepoc.com';
  api = `${this.serverName}/api/v1/`;
  url = `${this.serverName}/app/main#/dashboards/61e7f498559ce1002bba8680`;
  username = 'sebastienm@jump-biz.com';
  password = 'Azerty31*';

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.loadSisense();
  }

  loadSisense(): void {
    Sisense.connect(this.serverName, false)
      .then((app: any) => {
        app.dashboards.load('61e7f498559ce1002bba8680').then((dash: any) => {
          const widget1 = (dash.widgets.get(
            '61f40dc3559ce1002bba8843'
          ).container = document.getElementById('widget1'));
          dash.widgets.get('61eacfde559ce1002bba873c').container =
            document.getElementById('widget2');
          dash.renderFilters(document.getElementById('filters'));
          dash.refresh();
        });
      })
      .catch((err: any) => console.log(err));
  }
}
