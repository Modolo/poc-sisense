require 'C:\php\vendor\autoload.php';

//load the JWT library?

use \Firebase\JWT\JWT;

//The user session information is passed from the Identity Provider in a session cookie that will contain the user.

==============================LOG IN===================================

// 1. Call to the function that extracts the User passed in the cookie

Insert code here to extract the session user information into a userID variable. In this sample code we are hardcoding the user value.

Var User_id = 'john.doe@sisense.com'

// 2. Setup token and assign values to its parameters

$jwtToken = constructToken($userid);

print($jwtToken);

// 3. Encode JWT object into a token

$encodedToken = encodeJWT ($jwttoken)

//4. Setup Redirect to the customer login page

$redirectURL = setupRedirectUrl (@encodedToken)

//5. Perform Redirect to the Sisense site with the given token

header("Location: https://yoursite.sisense.com/jwt?jwt=".$jwtToken."&".$returnToVal);

==============================Functions============================

//Function: Create and assign values to the token

function constructToken($userid)

{

$token = array (

'sub' => $userid,

'jti' => time(),

'iat' => time()

);

}

//Function: Encode the token

encodeJWT($token)

{

//

//Note the secret can be hardcoded into the handler code and is retrieved from the SSO settings in Sisense, or a GET function into the Sisense Rest API can be used at the /api/settings/sso endpoint

$secret = "YOUR SECRET";

//Create Token

$jwt = JWT::encode($token, $secret);

}

setupRedirectUrl ($encodedToken)

//The query string is retrieved from the URL and can include authentication information, a redirect URL, or other information.

$querystring = $_SERVER['QUERY_STRING'];

print($querygtring);
