//Function that will require an HTTP cookie (context) to be passed to as an input

public void ProcessRequest(HttpContext context)

{

// 1. Call to the function that extracts the User passed in the cookie

string userID = getUserID(context);



// If user is not logged in, redirect to main login page

if(username == null)

{

context.Response.Redirect("./default.aspx");

}

// 2. Setup token and assign values to its parameters

var payload = constructToken(userID);



// 3. Encode JWT object into a token

String encodedToken = encodeJWT(payload)

//4. Setup Redirect to the customer login page

string redirectUrl = setupRedirectUrl(encodedToken)

//5. Perform Redirect

context.Response.Redirect(redirectUrl);

==============================Functions=================================================

//Function: Extract user from session cookie

public string getUserID(HttpContext context)

{

// Extract the user from the cookie

var Cookie = context.Request.Cookies["dummyUser"];

// Return the cookie's value if it exists

if ((Cookie != null) && (Cookie.Value != null))

return Cookie.Value;

return null;

}

//Function: Create and assign values to the token

public System.Collections.Generic.Dictionary

{

//Assign the value of the offset of current time from Epoch time.

TimeSpan timeSinceEpoch = (DateTime.UtcNow - new DateTime(1970, 1, 1));

int secondsSinceEpoch = (int)timeSinceEpoch.TotalSeconds;



//Setup and assign values to token

var payload = new System.Collections.Generic.Dictionary

{ "iat", secondsSinceEpoch },

{ "sub", username },

{ "jti", Guid.NewGuid() }

};

return payload;

}

//Function: Encode the token

public string encodeJWT(System.Collections.Generic.Dictionary

{

//

//Note the secret can be hardcoded into the handler code and is retrieved from the SSO settings in Sisense, or a GET function into the Sisense Rest API can be used at the /api/settings/sso endpoint.

string secret = "

string token = JWT.JsonWebToken.Encode(payload, secret, JWT.JwtHashAlgorithm.HS256);

return token;

//Function: Sets up the redirect URL including the toke

public string setupRedirectURL(token)

{

// This is the Sisense URL which can handle (decode and process) the JWT token

String redirectUrl = "http://reporting.mytestapp.com:8081/jwt?jwt=" + token;



// Which URL the user was initially trying to open

string returnTo = context.Request.QueryString["return_to"];

if (returnTo != null)

{

redirectUrl += "&return_to=" + HttpUtility.UrlEncode(returnTo);

}

return redirectURL;

}

$('#logout').click(() => {

// Log out by navigating the iFrame to the Logout API

$('#frame1').attr('src', 'http://reporting.mytestapp.com:8081/api/auth/logout');



// Remove the user's cookie

Cookies.remove('dummyUser');



// Navigate to the main page

window.location.href = '/default.aspx';

});
